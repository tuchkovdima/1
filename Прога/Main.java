package binarytree;

import binarytree.Node;

public class Main
{
    public static void main(String[] args)
    {
        BinaryTree binaryTree = new BinaryTree();

        //добавление элементов
        binaryTree.insertNode(50);
        binaryTree.insertNode(80);
        binaryTree.insertNode(40);
        binaryTree.insertNode(39);
        binaryTree.insertNode(41);
        binaryTree.insertNode(37);
        binaryTree.insertNode(44);
        binaryTree.insertNode(38);
        binaryTree.insertNode(36);
        binaryTree.insertNode(75);
        binaryTree.insertNode(85);
        binaryTree.insertNode(71);
        binaryTree.insertNode(70);
        binaryTree.insertNode(87);
        binaryTree.insertNode(88);

        binaryTree.deleteNode(88);
        System.out.println("Элемент найден: " + binaryTree.contains(71));
        binaryTree.printTree();
    }
}
